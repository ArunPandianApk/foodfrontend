import React from 'react';
import Header from './Header.js';
import './App.css';

export default class EndedMessage extends React.Component
{
	componentWillMount()
	{
		fetch('http://ajira-foodbackend.herokuapp.com/orders/session')
			.then(function(session) {return session.json()})
			.then(session => this.setState({session}));
	}

	getCurrentDate = () =>
	{
		var date=new Date();
		date.setUTCHours(0,0,0,0);
		return date.toISOString().substring(0,10);
	}

	render()
	{
		if(this.state !== null && this.state.session !== undefined)
		{
			if(!this.state.session.isEnded)
				window.location = '/';
			return(
				<div id='endedmessagepage'>
				<Header />
					<h3 id='pageheading'>Today's Session has Ended!!!</h3>
				</div>
				);
		}
		return <h3>Loading......</h3>
	}
}
