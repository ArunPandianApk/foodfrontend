import React from 'react';
import './App.css';
import Header from './Header.js';
import Util from './Util.js';

export default class Login extends React.Component
{
	login = async () =>
	{
		var obj =
		{
			id: 1,
	    	name: Util.getValue('usernameadmin'),
	    	password: Util.getValue('passwordadmin')
	    };
			fetch("http://ajira-foodbackend.herokuapp.com/superuser/login",
	    {
	    method: 'POST',
			credentials: 'include',
	    headers:
	    {
	        'Accept' : "text/plain",
		    'Content-Type' : 'application/json'
	    },
	    body: JSON.stringify(obj)
	    })
	    .then(response => response.text())
	    .then(function(response)
	    {
	    	if(response === "true")
					window.location='/admin';
			else
				window.location='/login';
	    });
	}

	cancel = () =>
	{
		window.location = '/';
	}

	enter = (event) =>
	{
		if(event.key === 'Enter')
			this.login();
	}

	render()
	{
		return(
			<div id='loginpage'>
				<Header />
				<center id='login' onKeyPress={this.enter}>
					<h3 id='pageheading'>Login</h3>
					Username:&nbsp;
					<input type='text' id='usernameadmin' autoComplete='off'/>
					<br />
					<br />
					Password:&ensp;
					<input type='password' id='passwordadmin' />
					<br />
					<br />
					<input type='submit' id='loginbutton1' value='Login' className='buttons' onClick={this.login} />
					&ensp;
					<input type='reset' value='Cancel' className='buttons' onClick={this.cancel} />
				</center>
			</div>
			);
	}
}
