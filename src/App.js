import React from 'react';
import Home from './Home.js';
import Login from './Login.js';
import Order from './Order.js';
import AdminPage from './AdminPage.js';
import {BrowserRouter, Route} from 'react-router-dom';
import ChangePassword from './ChangePassword.js';
import EditUsers from './EditUsers.js';
import EditFoods from './EditFoods.js';
import EndedMessage from './EndedMessage.js';
import VendorLogin from './VendorLogin.js';
import AddUsers from './AddUsers.js';
import AddFoods from './AddFoods.js';
import ViewUsers from './ViewUsers.js';
import ViewFoods from './ViewFoods.js';
import EndSession from './EndSession.js';
import './App.css';

export default class App extends React.Component
{
	render()
	{
		return(
			<BrowserRouter>
				<div>
					<Route exact path='/' component={Home} />
					<Route path='/login' component={Login} />
					<Route path='/order' component={Order} />
					<Route path='/admin' component={AdminPage} />
					<Route path='/changepassword/:id' component={ChangePassword} />
					<Route path='/edituser' component={EditUsers} />
					<Route path='/editfood' component={EditFoods} />
					<Route path='/endedmessage' component={EndedMessage} />
					<Route path='/vendorlogin' component={VendorLogin} />
					<Route path='/addusers' component={AddUsers} />
					<Route path='/addfoods' component={AddFoods} />
					<Route path='/viewusers' component={ViewUsers} />
					<Route path='/viewfoods' component={ViewFoods} />
					<Route path='/endsession' component={EndSession} />
				</div>
			</BrowserRouter>
			);
	}
}
