import React from 'react';
import DropDown from './DropDown.js';
import Util from './Util.js';
import './App.css';

export default class Form extends React.Component
{
	onSubmit = async () =>
	{
		var username = Util.getValue('username');
		var quantity = Util.getValue('userquantity');
		if(username !== '' && quantity !=='')
			this.proceedToAdd();
		else
			alert('Username or Quantity cannot be Empty');
	}

	proceedToAdd = async () =>
	{
		var that = this;
		await fetch('http://ajira-foodbackend.herokuapp.com/users/'+Util.getValue('username'))
					 .then(user => user.json())
					 .then(function(user)
					 {
						 var obj = {
							userId: user.id,
							quantity: Util.getValue('userquantity')
						}
						that.checkAdd(obj);
					});
	}

	checkAdd = async (obj) =>
	{
		var that = this;
		await fetch('http://ajira-foodbackend.herokuapp.com/consumption/check/'+obj.userId)
					.then(response => response.text())
					.then(function(response)
					{
						if(response === 'true')
						{
							that.postConsumption(obj);
							alert('Thank you!!! Your Response has been recorded!!!');
						}
						else
							alert('You have already given you Response!!!');
					});
	}

	postConsumption = (obj) =>
	{
		fetch('http://ajira-foodbackend.herokuapp.com/consumption/add',
		{
	      	method: 'POST',
	      	headers:
	      	{
	        	'Accept' : 'application/json',
	        	'Content-Type' : 'application/json'
	      	},
	      	body: JSON.stringify(obj)
	    });
	}

	endSession = async () =>
	{
		window.location = '/endsession';
	}

	enter = (event) =>
	{
		if(event.key === 'Enter')
			this.onSubmit();
	}

	render()
	{
		return(
			<div id='consumptionpage'>
				<div id='consumptionform'>
					<h3 id='pageheading'>Consumption Details</h3>
						Name:&ensp;&ensp;&ensp;
						<select id='username'>
							<option value=''></option>
							{
								this.props.nameList.map((name,i) => <DropDown key={i} data={name} />)
							}
						</select>
						<br />
						<br />
						Quantity:&ensp;
						<input type='text' id='userquantity' className='textbox' autoComplete='off' onKeyPress={this.enter} />
						<br />
						<br />
						<input type='button' value='Submit' className='buttons'  onClick={this.onSubmit} />
					<br /><br />
					<input type='button' value='End Session' className='buttons' onClick={this.endSession}/>
				</div>
			</div>
			);
	}
}
