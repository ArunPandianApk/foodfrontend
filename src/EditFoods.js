import React from 'react';
import AdminHeader from './AdminHeader.js';
import Util from './Util.js';
import './App.css';

export default class EditFoods extends React.Component
{
  state = {
    isAuthenticated: false
  }

  componentWillMount()
	{
		var that = this;
		fetch('http://ajira-foodbackend.herokuapp.com/superuser/check/1', { credentials: "include" })
	    .then(response => response.text())
	    .then(function(response)
	    {
	    	if(response === "true")
          that.getFoodsList();
  			else
  				window.location = '/login';
	    });
	}

  getFoodsList = async () =>
  {
    var that = this;
    await fetch('http://ajira-foodbackend.herokuapp.com/item/all')
    .then(foodsList => foodsList.json())
    .then(function(foodsList){
      that.setState({
        foodsList,
        isAuthenticated: true
      });
    });
  }

  checkPassword = async () =>
  {
    var that = this;
    await fetch('http://ajira-foodbackend.herokuapp.com/superuser/1')
          .then(admin => admin.json())
          .then(function(admin)
          {
            if(admin.password === Util.getValue('editfoodpassword'))
              that.changeFood();
            else
            {
              fetch('http://ajira-foodbackend.herokuapp.com/superuser/logout')
              .then(function(){
                window.location = '/login';
                alert('Wrong Admin Password!!! You are being redirected to Login Page!!!');
              });
            }
          });
  }

  getObject = () =>
  {
    return {
      id: Util.getValue('editfoodid'),
      name: Util.getValue('neweditname'),
			isDelivered: true
    }
  }

  back = () =>
  {
    window.location = '/admin';
  }

  changeFood = () =>
  {
    fetch("http://ajira-foodbackend.herokuapp.com/item/add",
		{
	      	method: 'POST',
	      	headers:
	      	{
	        	'Content-Type' : 'application/json'
	      	},
	      	body: JSON.stringify(this.getObject())
	    })
      .then(function()
      {
        alert('Food name changed to '+Util.getValue('neweditname')+'!!!!');
        window.location = '/editfood';
      });
  }

  onEnter = (event) =>
  {
    if(event.key === 'Enter')
			this.checkPassword();
  }

  render()
  {
    if(this.state.isAuthenticated)
    {
      return (
        <div id='editfoodpage'>
          <AdminHeader />
          <div id='editfood' onKeyPress={this.onEnter}>
            <h3>Edit Foods</h3>
            Select Food:&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;
            <select id='editfoodid'>
            <option />
            {
                this.state.foodsList.map((food,i) => <option key={i} value={food.id}>{food.name}</option>)
            }
            </select>
            <br /><br />
            Enter new name:&ensp;&ensp;<input type='text' id='neweditname' />
            <br /><br />
            Admin Password:&ensp;<input type='password' className='textbox' id='editfoodpassword' />
            <br /><br />
            <input type='button' className='buttons' value='Confirm' onClick={this.checkPassword} />&ensp;
            <input type='button' className='buttons' value='Back' onClick={this.back} />
          </div>
        </div>
        );
      }
      return <h3>Loading.....</h3>;
  }
}
