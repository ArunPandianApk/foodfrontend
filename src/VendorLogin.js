import React from 'react';
import './App.css';
import Header from './Header.js';
import Util from './Util.js';

export default class VendorLogin extends React.Component
{
	onClick = async () =>
	{
	    var obj =
	    {
	    	id: 2,
	    	password: Util.getValue("vendorPassword"),
	    	name: Util.getValue("vendorname")
	    }
	    await fetch('http://ajira-foodbackend.herokuapp.com/superuser/login',
	    {
	    	method: 'POST',
				credentials: "include",
	    	headers:
		    {
		        'Accept' : "text/plain",
			    'Content-Type' : 'application/json'
		    },
		    body: JSON.stringify(obj)
		 })
	    .then(response => response.text())
	    .then(function(response)
	    {
	    	if(response === "true")
					window.location='/order';
			else
				alert('Login Failed');
	    });
	}

	enter = (event) =>
	{
		if(event.key === 'Enter')
			this.onClick();
	}

	render()
	{
		return(
			<div id='vendorloginpage'>
			<Header />
			<div id='vendorloginform' onKeyPress={this.enter}>
				<center>
		            <h3 id="pageheading">The Session has not yet been Started!!!</h3>
		            Username:&nbsp;<input type='text' id='vendorname' autoComplete='off' className='textbox' />
					<br />
		            <br />
		            Password:&ensp;<input id='vendorPassword' type='password' className='textbox' />
		            <br />
		            <br />
		            <input type='button' className='buttons' id='vendorLoginButton' value ='Start Session' onClick={this.onClick} />
	        	</center>
	        </div>
			</div>
          );
	}
}
