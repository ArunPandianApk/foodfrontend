import React from 'react';
import Util from './Util.js';
import './App.css';

export default class RetryUser extends React.Component
{
	check = async () =>
  {
    var password = Util.getValue('retryuserpassword');
    const admin = await fetch('http://ajira-foodbackend.herokuapp.com/superuser/1')
								.then(admin => admin.json());
    if(admin.password === password)
    {
      Util.hideContent('retryusercomponent');
      this.props.addFunction();
    }
    else
    {
      alert('You have entered Wrong Password again!!!\nRedirecting to Login Page!!!');
      fetch("http://ajira-foodbackend.herokuapp.com/superuser/logout");
      window.location = '/login';
    }
  }

  enter = (event) =>
	{
		if(event.key === 'Enter')
			this.check();
	}

  cancel = () =>
  {
    window.location = '/admin';
  }

  render()
  {
    return (
    <div id='retryusercomponent'>
      <h3>Retry Password</h3>
      <p>You have entered a Wrong Password!!! Enter the Correct Password to Proceed!!! </p>
      Password:&ensp; <input type='password' id='retryuserpassword' onKeyPress={this.enter}/><br /><br />
      <input type='button' value='Proceed' className='buttons' onClick={this.check} />&ensp;
      <input type='button' value='Cancel' className='buttons' onClick={this.cancel} />
    </div>
    );
  }
}
