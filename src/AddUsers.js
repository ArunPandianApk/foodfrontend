import React,{Component} from 'react';
import SameUsernamePopup from './SameUsernamePopup.js';
import Util from './Util.js';
import AdminHeader from './AdminHeader.js';
import AdminButtons from './AdminButtons.js';
import RetryUser from './RetryUser.js';
import './App.css';

export default class AddUsers extends Component
{
	state = {
    isAuthenticated: false
  }

  componentWillMount()
	{
		var that = this;
		fetch('http://ajira-foodbackend.herokuapp.com/superuser/check/1', { credentials: "include" })
	    .then(response => response.text())
	    .then(function(response)
	    {
	    	if(response === "true")
          that.setState({isAuthenticated: true});
  			else
  				window.location = '/login';
	    });
	}

	checkPassword = (adminPassword) =>
	{
		var password = Util.getValue('confirmpassword1');
		if(adminPassword !== password)
		{
			Util.showContent('retryusercomponent');
			Util.hideContent('addusers');
			return false;
		}
		return true;
	}

	proceedToAdd = () =>
	{
		var obj=
		{
			name: Util.getValue('newusername'),
			isActive: true
		}
		this.postUsers(obj);
		alert('User '+obj.name+' is Added');
		this.clear();
		window.location = '/addusers';
	}

	checkAdd = async () =>
	{
		const usersList = await fetch('http://ajira-foodbackend.herokuapp.com/users/all')
								.then(response => response.json());
		const admin = await fetch('http://ajira-foodbackend.herokuapp.com/superuser/1')
								.then(admin => admin.json());
		if(this.checkPassword(admin.password))
			this.doesUserExists(usersList);
	}

	doesUserExists = (usersList) =>
	{
		var newusername = Util.getValue('newusername');
		for(var i=0; i < usersList.length; i++)
			if(usersList[i].name === newusername)
			{
				Util.showContent('usernamepopup');
				Util.hideContent('addusers');
				return true;
			}
		this.proceedToAdd();
	}

	clear = () =>
	{
		Util.empty('newusername');
		Util.empty('confirmpassword1');
		this.setState({});
	}

	postUsers = (obj) =>
	{
		fetch("http://ajira-foodbackend.herokuapp.com/users/add",
		{
	      	method: 'POST',
	      	headers:
	      	{
	        	'Content-Type' : 'application/json'
	      	},
	      	body: JSON.stringify(obj)
	    });
	}

	cancel = () =>
	{
		window.location = '/admin';
	}

	enter = (event) =>
	{
		if(event.key === 'Enter')
			this.checkAdd();
	}

	render()
	{
		if(this.state.isAuthenticated)
		{
			return(
				<div id='viewfoodspage'>
					<AdminHeader />
					<AdminButtons />
					<div id='addusers' onKeyPress={this.enter}>
						<h3 id="pageheading">Add User</h3>
						User's Name:&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<input type='text' id='newusername' autoComplete='off' />
						<br />
						<br />
						Confirm Password: &ensp;<input type='password' id='confirmpassword1' />
						<br />
						<br />
						<input type='submit' className='buttons' value='Add' onClick={this.checkAdd} />&ensp;
						<input type='reset' className='buttons' value='Cancel' onClick={this.cancel} />&ensp;
					</div>
					<SameUsernamePopup username={this.getUsername} addFunction={this.proceedToAdd} clearFunction={this.clear} />
					<RetryUser addFunction={this.proceedToAdd}/>
				</div>
			);
		}
		return <h3>Loading.....</h3>;
	}
}
