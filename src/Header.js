import React from 'react';
import './App.css';

class Header extends React.Component
{
	login = () =>
	{
		window.location='/login';
	}

	goHome = () =>
	{
		window.location='/';
	}

	render()
	{
		return(
			<div id='header'>
				<strong>Ajira</strong>
				<input type='button' value='Login' className='headerbutton' id='loginbutton' onClick={this.login}/>
				<input type='button' value='Home' className='headerbutton' id='homebutton' onClick={this.goHome}/>
			</div>
			)
	}
}

export default Header;
