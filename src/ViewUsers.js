import React from 'react';
import UserTable from './UserTable.js';
import AdminHeader from './AdminHeader.js';
import AdminButtons from './AdminButtons.js';
import './App.css';

export default class ViewUsers extends React.Component
{
	state = {
    isAuthenticated: false
  }

  componentWillMount()
	{
		var that = this;
		fetch('http://ajira-foodbackend.herokuapp.com/superuser/check/1', { credentials: "include" })
	    .then(response => response.text())
	    .then(function(response)
	    {
	    	if(response === "true")
          that.setState({isAuthenticated: true});
  			else
  				window.location = '/login';
	    });
	}

	constructor(props)
	{
		super(props);
		this.getUsersList();
	}

	componentWillReceiveProps(someProp)
	{
		fetch('http://ajira-foodbackend.herokuapp.com/users/all')
		.then(usersList => usersList.json())
		.then(usersList => this.setState({usersList}));
	}

	getUsersList = async () =>
	{
		await fetch('http://ajira-foodbackend.herokuapp.com/users/all')
			  .then(usersList => usersList.json())
			  .then(usersList => this.setState({usersList}));
	}

	manipulateUsers = async (action, id) =>
	{
		var url = 'http://ajira-foodbackend.herokuapp.com/users/'+action+'/'+id;
		await fetch(url);
		this.getUsersList();
	}

	onClick = () =>
	{
		window.location = '/edituser';
	}

	render()
	{
		if(this.state.isAuthenticated && this.state.usersList !== undefined)
			return(
				<div id='viewfoodspage'>
					<AdminHeader />
					<AdminButtons />
					<div id='viewusers'>
						<center>
							<h3 id='pageheading'>User Details</h3>
							<input type='button' className='buttons' onClick={this.onClick} value='Edit Users' />
							<br /><br />
							<UserTable list={this.state.usersList} manipulate={this.manipulateUsers} />
						</center>
					</div>
				</div>
			);
		return(<h3 id='viewusers'>Loading........</h3>);
	}
}
