import './App.js';
import React from 'react';

export default class AdminButtons extends React.Component
{
  addUsers = () =>
	{
		window.location = '/addusers';
	}

	viewUsers = () =>
	{
		window.location = '/viewusers';
	}

	addFoods = () =>
	{
		window.location = '/addfoods';
	}

	viewFoods = () =>
	{
    window.location = '/viewfoods';
	}

	viewReport = () =>
	{
		window.location = '/admin';
	}

  render()
  {
    return (<div id='adminbuttons'>
      <h3>Admin Page</h3>
      <input type='button' className='buttons' value='Report' onClick={this.viewReport} />&ensp;
      <input type='button' className='buttons' value='Add Users' onClick={this.addUsers} />&ensp;
      <input type='button' className='buttons' value='View Users' onClick={this.viewUsers} />&ensp;
      <input type='button' className='buttons' value='Add Food Items' onClick={this.addFoods} />&ensp;
      <input type='button' className='buttons' value='View Food Items' onClick={this.viewFoods} />
      <br /><br /><hr width="75%"/>
    </div>);
  }
}
