import React from 'react';
import SummaryTable from './SummaryTable.js';
import './App.css';
import Util from './Util.js';

export default class Report extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {
			summary: null,
			overall: null
		}
		this.getSummary();
		this.getOverallSummary();
	}

	getSummary = async () =>
	{
		await fetch('http://ajira-foodbackend.herokuapp.com/report/summary')
		.then(summary => summary.json())
		.then(summary => this.setState({summary}));
	}

	getOverallSummary = async () =>
	{
		await fetch('http://ajira-foodbackend.herokuapp.com/report/overallsummary')
		.then(overall => overall.json())
		.then(overall => this.setState({overall}));
	}

	viewSummary = () =>
	{
		Util.showContent('reportfordays');
		Util.hideContent('overallreport');
	}

	overallSummary = () =>
	{
		Util.hideContent('reportfordays');
		Util.showContent('overallreport');
	}

	render()
	{
		if(this.state.summary === null || this.state.overall === null)
			return(
				<div id='reportcomponent'>
					<h3>Loading......</h3>
				</div>);
		return(
			<div id='reportcomponent'>
				<center>
					<h3 id='pageheading'>Reports</h3>
					<input type='button' className='buttons' value='Overall Report' onClick={this.overallSummary}/>&ensp;
					<input type='button' className='buttons' value='Report for 30 Days' onClick={this.viewSummary}/>
					<SummaryTable values={this.state.summary} id='reportfordays' title='Report For Last 30 Days' />
					<SummaryTable values={this.state.overall} id='overallreport' title='Overall Report' />
				</center>
			</div>
			);

	}
}
