import React from 'react';
import FoodTable from './FoodTable.js';
import AdminHeader from './AdminHeader.js';
import AdminButtons from './AdminButtons.js';
import './App.css';

export default class FoodUsers extends React.Component
{
	state = {
    isAuthenticated: false
  }

  componentWillMount()
	{
		var that = this;
		fetch('http://ajira-foodbackend.herokuapp.com/superuser/check/1', { credentials: "include" })
	    .then(response => response.text())
	    .then(function(response)
	    {
	    	if(response === "true")
          that.setState({isAuthenticated: true});
  			else
  				window.location = '/login';
	    });
	}

	constructor(props)
	{
		super(props);
		this.getFoodList();
	}

	componentWillReceiveProps(someProp)
	{
		fetch('http://ajira-foodbackend.herokuapp.com/item/all')
		.then(foodList => foodList.json())
		.then(foodList => this.setState({foodList}));
	}

	getFoodList = async () =>
	{
		await fetch('http://ajira-foodbackend.herokuapp.com/item/all')
			  .then(foodList => foodList.json())
			  .then(foodList => this.setState({foodList}));
	}

	manipulateFoods = async (action, id) =>
	{
		var url = 'http://ajira-foodbackend.herokuapp.com/item/'+action+'/'+id;
		await fetch(url);
		this.getFoodList();
	}

	onClick = () =>
	{
		window.location = '/editfood';
	}

	render()
	{
		if(this.state.isAuthenticated && this.state.foodList !== undefined)
			return(
				<div id='viewfoodspage'>
					<AdminHeader />
					<AdminButtons />
					<div id='viewfoods'>
						<center>
							<h3 id='pageheading'>Food Details</h3>
							<input type='button' className='buttons' onClick={this.onClick} value='Edit Foods' />
							<br /><br />
							<FoodTable list={this.state.foodList} manipulate={this.manipulateFoods} />
						</center>
					</div>
				</div>
			);
		return(<h3>Loading........</h3>);
	}
}
