import React from 'react';
import SameFoodnamePopup from './SameFoodnamePopup.js';
import './App.css';
import AdminHeader from './AdminHeader.js';
import AdminButtons from './AdminButtons.js';
import Util from './Util.js';
import RetryFood from './RetryFood.js';

export default class AddFood extends React.Component
{
	state = {
    isAuthenticated: false
  }

  componentWillMount()
	{
		var that = this;
		fetch('http://ajira-foodbackend.herokuapp.com/superuser/check/1', { credentials: "include" })
	    .then(response => response.text())
	    .then(function(response)
	    {
	    	if(response === "true")
          that.setState({isAuthenticated: true});
  			else
  				window.location = '/login';
	    });
	}

	checkPassword = (adminPassword) =>
	{
		var password = Util.getValue('confirmpassword2');
		if(adminPassword !== password)
		{
			Util.showContent('retryfoodcomponent');
			Util.hideContent('addfoods');
			return false;
		}
		return true;
	}

	proceedToAdd = () =>
	{
		var obj=
		{
			name: Util.getValue('newfoodname'),
			isDelivered: true
		}
		this.postFood(obj);
		alert('Food '+obj.name+' is Added');
		this.clear();
		window.location = '/addfoods';
	}

	checkAdd = async () =>
	{
		const foodsList = await fetch('http://ajira-foodbackend.herokuapp.com/item/all')
								.then(response => response.json());
		const admin = await fetch('http://ajira-foodbackend.herokuapp.com/superuser/1')
								.then(admin => admin.json());
		if(this.checkPassword(admin.password))
			this.doesFoodExists(foodsList);
	}

	doesFoodExists = (foodsList) =>
	{
		var newfoodname = Util.getValue('newfoodname');
		for(var i=0; i < foodsList.length; i++)
			if(foodsList[i].name === newfoodname)
			{
				Util.showContent('foodnamepopup');
				Util.hideContent('addfoods');
				return true;
			}
		this.proceedToAdd();
	}

	clear = () =>
	{
		Util.empty('newusername');
		Util.empty('confirmpassword1');
		this.setState({});
	}

	postFood = (obj) =>
	{
		fetch("http://ajira-foodbackend.herokuapp.com/item/add",
		{
	      	method: 'POST',
	      	headers:
	      	{
	        	'Content-Type' : 'application/json'
	      	},
	      	body: JSON.stringify(obj)
	    });
	}

	enter = (event) =>
	{
		if(event.key === 'Enter')
			this.checkAdd();
	}

	cancel = () =>
	{
		window.location = '/admin';
	}

	render()
	{
		return(
			<div>
				<AdminHeader />
				<AdminButtons />
				<div id='addfoods' onKeyPress={this.enter}>
					<h3 id='pageheading'>Add Food</h3>
					Food Name:&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;
					<input type='text' id='newfoodname' autoComplete='off' />
					<br />
					<br />
					Confirm Password: &ensp;<input type='password' id='confirmpassword2' />
					<br />
					<br />
					<input type='submit' className='buttons' value='Add' onClick={this.checkAdd} />&ensp;
					<input type='reset' className='buttons' value='Cancel' onClick={this.cancel} />&ensp;
				</div>
				<SameFoodnamePopup foodname={this.getFoodname} addFunction={this.proceedToAdd} clearFunction={this.clear} />
				<RetryFood proceedToAdd={this.proceedToAdd}/>
			</div>
			);
	}
}
