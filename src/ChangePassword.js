import React from 'react';
import './App.css';
import AdminHeader from './AdminHeader.js'
import Util from './Util.js';

export default class ChangePassword extends React.Component
{
  state = {
    isAuthenticated: false
  }

  componentWillMount()
	{
		var that = this;
    var id = this.props.match.params.id;
		fetch('http://ajira-foodbackend.herokuapp.com/superuser/check/'+id, { credentials: "include" })
	    .then(response => response.text())
	    .then(function(response)
	    {
	    	if(response === "true")
          that.setState({isAuthenticated: true});
  			else
  				id === '1' ? window.location = '/login' : window.location = '/';
	    });
	}

  onSubmit = async () =>
  {
    await fetch("http://ajira-foodbackend.herokuapp.com/superuser/change",
    {
      method: 'POST',
      credentials: "include",
	    headers:
	    {
	      'Accept' : "text/plain",
		    'Content-Type' : 'application/json'
	    },
	    body: JSON.stringify(this.getObject())
    })
    .then(response => response.text())
    .then(function(response)
    {
      if(response === "true")
      {
        alert("Password Successfully Changed!!!");
        window.location = document.referrer;
      }
      else
        alert("Invalid Username or Password!!!");
    })
  }

  getObject = () =>
  {
    return({
      username: Util.getValue('currentusername'),
      password: Util.getValue('currentpassword'),
      newpassword: Util.getValue('newpassword')
    });
  }

  enter = (event) =>
	{
		if(event.key === 'Enter')
			this.onSubmit();
	}

  cancel = () =>
	{
		window.location = '/admin';
	}

  render()
  {
    console.log(this.state);
    if(this.state.isAuthenticated)
    {
      return(
        <div id='changepasswordpage'>
          <AdminHeader />
          <div id='changepasswordform'>
            <h3 id='pageheading'>Change Password</h3>
            <form onKeyPress={this.enter} >
              Username: &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<input type='text' id='currentusername' className='textbox' />
              <br /><br />
              Current Password: &ensp;&nbsp;<input type='password' id='currentpassword' className='textbox' />
              <br /><br />
              New Password: &ensp;&ensp;&ensp;&ensp;<input type='password' id='newpassword' className='textbox' />
              <br /><br />
              <input type='button' className='buttons' value='Submit' onClick={this.onSubmit} />&nbsp;
              <input type='button' className='buttons' value='Cancel' onClick={this.cancel}/>
            </form>
          </div>
        </div>
      );}
      return <h3>Loading......</h3>;
  }
}
