import React from 'react';
import './App.css';

export default class FoodTableContent extends React.Component
{
	stopFood = () =>
	{
		var id = this.props.data.id;
		this.props.manipulate('stopdeliver',id)
	}

	deliverFood = () =>
	{
		var id = this.props.data.id;
		this.props.manipulate('deliver',id)
	}

	render()
	{
		return(
			<tr>
				<td>{this.props.data.name}</td>
				<td>{this.props.data.isDelivered ? 'Yes' : 'No'}</td>
				<td>
				{
					this.props.data.isDelivered ? <input type='button' value='Stop Delivering' className='buttons' onClick={this.stopFood} />
											 : <input type='button' value='Restart Delivering' className='buttons' onClick={this.deliverFood} />
				}
				</td>
			</tr>
			)
	}
}