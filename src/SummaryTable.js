import React from 'react';
import SummaryTableContent from './SummaryTableContent.js';
import './App.css';

export default class SummaryTable extends React.Component
{
	displayData = () =>
	{
		if(this.props.values.length !== 0)
			return this.props.values.map((row,i) => <SummaryTableContent key={i} data={row} />);
		return (<tr><td colSpan={4}>No data to Display</td></tr>);
	}

	render()
	{
		return(
			<div className='reporttablecontent' id={this.props.id}>
				<h4>{this.props.title}</h4>
				<table>
					<thead>
						<tr>
							<th>Food Name</th>
							<th>Average Consumption Per Person</th>
							<th>No of Days Served</th>
							<th>No of Persons Served</th>
						</tr>
					</thead>
					<tbody>
					{ this.displayData() }
					</tbody>
				</table>
			</div>
			)
	}
}
