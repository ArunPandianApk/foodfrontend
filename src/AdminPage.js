import React from 'react';
import AdminHeader from './AdminHeader.js';
import Report from './Report.js';
import AdminButtons from './AdminButtons.js';
import './App.css';

export default class AdminPage extends React.Component
{
	state = {
		isAuthenticated: false
	}

	componentWillMount()
	{
		var that = this;
		fetch("http://ajira-foodbackend.herokuapp.com/superuser/check/1", { credentials: "include" })
	    .then(response => response.text())
	    .then(function(response)
	    {
	    	if(response === "true")
					that.setState({ isAuthenticated: true });
				else
					window.location = '/login';
	    });
	}

	render()
	{
		if(this.state.isAuthenticated)
		{
			return(
				<div id='adminpage'>
					<AdminHeader />
					<AdminButtons />
					<Report />
				</div>
				);
		}
		return <h3>Loading.....</h3>;
	}
}
