import React from 'react';
import Header from './Header.js';
import Util from './Util.js';
import './App.css';

export default class EndSession extends React.Component
{
  componentWillMount()
	{
		fetch('http://ajira-foodbackend.herokuapp.com/orders/session')
			.then(function(session) {return session.json()})
			.then(session => this.setState({session}));
	}

	getCurrentDate = () =>
	{
		var date=new Date();
		date.setUTCHours(0,0,0,0);
		return date.toISOString().substring(0,10);
	}

	cancel = () =>
	{
		window.location = '/';
	}

	validate = async () =>
	{
		var username = Util.getValue('usernameend');
		var password = Util.getValue('passwordend');
		await fetch('http://ajira-foodbackend.herokuapp.com/superuser/1')
			  .then(response => response.json())
			  .then(async function(response)
			  {
			  		if(response.password === password && response.name === username)
			  			await fetch("http://ajira-foodbackend.herokuapp.com/orders/end");
			  		else
			  			alert("Wrong Password!!!! Can not end Session!!!!");
			  })
				.then(() => {window.location = '/';});
	}

	enter = (event) =>
	{
		if(event.key === 'Enter')
			this.validate();
	}

	checkSession = () =>
	{
			if(this.state.session.date !==null && !this.state.session.isEnded)
				return true;
			window.location = '/';
	}

	render()
	{
		if(this.state !== null && this.state.session !== undefined && this.checkSession())
		{
			return(
				<div id='endsessionpage'>
					<Header />
					<div id='endsession' onKeyPress={this.enter}>
						<h3>Enter the Admin Credentials to End!!!</h3>
						Username:&ensp;
						<input type='text' id='usernameend' autoComplete='off'/>
						<br />
						<br />
						Password:&ensp;
						<input type='password' id='passwordend' />
						<br />
						<br />
						<input type='submit' value='End Session' className='buttons' onClick={this.validate} />
						&ensp;
						<input type='reset' value='Cancel' className='buttons' onClick={this.cancel} />
					</div>
				</div>
				);
			}
			return <h3>Loading.......</h3>;
	}
}
