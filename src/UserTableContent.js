import React from 'react';
import './App.css';

export default class UserTableContent extends React.Component
{
	removeUser = () =>
	{
		var id = this.props.data.id;
		this.props.manipulate('deactivate',id)
	}

	activateUser = () =>
	{
		var id = this.props.data.id;
		this.props.manipulate('activate',id)
	}

	render()
	{
		return(
			<tr>
				<td>{this.props.data.name}</td>
				<td>{this.props.data.isActive ? 'Active' : 'Not Active'}</td>
				<td>
				{
					this.props.data.isActive ? <input type='button' value='Deactivate' className='buttons' onClick={this.removeUser} />
											 : <input type='button' value='Activate' className='buttons' onClick={this.activateUser} />
				}
				</td>
			</tr>
			)
	}
}