import React from 'react';

export default class Util extends React.Component
{
  static hideContent(id)
  {
      document.getElementById(id).style.display='none';
  }

  static showContent(id)
  {
      document.getElementById(id).style.display='block';
  }

  static empty(id)
  {
    document.getElementById(id).value='';
  }

  static getValue(id)
  {
    return document.getElementById(id).value;
  }
}
