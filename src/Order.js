import React from 'react';
import Header from './Header.js';
import DropDown from './DropDown.js';
import './App.css';
import Util from './Util.js';

export default class Order extends React.Component
{
	state = {
		isAuthenticated: false,
		nameList: null
	}

	constructor(props)
	{
		super(props);
		this.getNames();
	}

	componentWillMount()
	{
		var that = this;
		fetch("http://ajira-foodbackend.herokuapp.com/superuser/check/2",{ credentials: "include" })
	    .then(response => response.text())
	    .then(function(response)
	    {
	    	if(response === "true")
				that.handleState(true);
			else
				window.location = '/';
	    });
	}

	getNames = async () =>
    {
      	await fetch("http://ajira-foodbackend.herokuapp.com/item/name")
			  .then(response => response.json())
			  .then(nameList => this.setState({nameList}));
    }

	handleState = (value) =>
	{
		this.setState({isAuthenticated: value});
	}

	onSubmitOrder = () =>
	{
	    var name = Util.getValue("itemName");
	    var quantity = Util.getValue("itemQuantity");
	    if(name !== '' && quantity !== '')
		    this.placeOrder(name);
		else
			alert("Foodname or Quantity cannot be Empty");
	}

	placeOrder = async (name) =>
	{
		var that = this;
		await fetch('http://ajira-foodbackend.herokuapp.com/item/'+name)
					 .then(item => item.json())
					 .then(function(item)
					 {
						 var obj = {
							 totalQuantity: Util.getValue("itemQuantity"),
							 itemId: item.id,
							 isEnded: false
						 }
							that.postObject(obj);
					 });
	}

	postObject = async (obj) =>
	{
	    await fetch("http://ajira-foodbackend.herokuapp.com/orders/add",
	    {
	    method: 'POST',
	    headers:
	    {
	        'Accept' : 'application/json',
		    'Content-Type' : 'application/json'
	    },
	    body: JSON.stringify(obj)
	    })
			.then(function(){
				window.location = '/';
			});
	}

	cancel = () =>
	{
		window.location = '/';
	}

	enter = (event) =>
	{
		if(event.key === 'Enter')
			this.onSubmitOrder();
	}

	changePassword = () =>
	{
		window.location = '/changepassword/2';
	}

	render()
	{
		if(this.state.nameList !== null && this.state.isAuthenticated)
		{
			return(
				<div id='orderspage'>
					<Header />
					<center id='orders'>
				            <h3 id='pageheading'>Enter Today's Details</h3>
				            Name:&ensp;&ensp;&ensp;
				            <select id='itemName'>
								<option />
								{
									this.state.nameList.map((name,i) => <DropDown key={i} data={name} />)
								}
							</select>
				            <br />
				            <br />
				            Quantity:&ensp;
				            <input id='itemQuantity' type='number' className='textbox' onKeyPress={this.enter}/>
				            <br />
				            <br />
				            <input type='submit' id='submitOrderButton' value='Start Session' className='buttons' onClick={this.onSubmitOrder} />
				            &ensp;
				            <input type='reset' id='cancelOrderButton' value='Cancel' className='buttons' onClick={this.cancel} />
									<br />
									<br />
									<input type='button' id='changePasswordButton' className='buttons' value='Change Password' onClick={this.changePassword} />
		          </center>
	          </div>
			);
		}
		return <h3>Loading.....</h3>;
	}
}
