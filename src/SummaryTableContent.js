import React from 'react';
import './App.css';

export default class SummaryTableContent extends React.Component
{
	render()
	{
		return(
			<tr>
				<td>{this.props.data.name}</td>
				<td>{this.props.data.averageConsumption}</td>
				<td>{this.props.data.numberOfDays}</td>
				<td>{this.props.data.numberOfPersons}</td>
			</tr>	
			)
	}
}