import React from 'react';
import './App.css';
import Util from './Util.js';

export default class SameFoodnamePopup extends React.Component
{
	cancel = () =>
	{
		Util.hideContent('foodnamepopup');
		Util.showContent('addfoods');
		this.props.clearFunction();
	}

	enter = (event) =>
	{
		if(event.key === 'Enter')
			this.this.props.addFunction();
	}

	render()
	{
		return(
			<div id='foodnamepopup'>
				<h3>Do you want to Add User??</h3>
				<div id='foodnamepopupcontent' onKeyPress={this.enter}>
					<p>
						The Food {this.props.getFoodname} already Exist!!!!
					</p>
					<input type='button' value='Add anyways!' onClick={this.props.addFunction} className='buttons' />&ensp;
					<input type='reset' value="Don't Add!" onClick={this.cancel} className='buttons' />
				</div>
			</div>
			);
	}
}
