import React from 'react';
import FoodTableContent from './FoodTableContent.js';
import './App.css';

export default class FoodTable extends React.Component
{
	displayData = () =>
	{
		console.log(this.props);
		if(this.props.list.length !== 0)
			return this.props.list.map((food,i) => <FoodTableContent key={i} data={food} manipulate={this.props.manipulate} />);
		return (<tr><td colSpan={3}>No data to Display</td></tr>);
	}

	render()
	{
		return(
			<table>
				<thead>
					<tr>
						<th>Food Name</th>
						<th>Currently Delivered</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				{ this.displayData() }
				</tbody>
			</table>
			);
	}
}
