import React from 'react';
import './App.css';
import Util from './Util.js';

export default class SameUsernamePopup extends React.Component
{
	cancel = () =>
	{
		Util.hideContent('usernamepopup');
		Util.showContent('addusers');
		this.props.clearFunction();
	}

	enter = (event) =>
	{
		if(event.key === 'Enter')
			this.props.addFunction();
	}

	render()
	{
		return(
			<div id='usernamepopup'>
				<h3>Do you want to Add User??</h3>
				<div id='usernamepopupcontent' onKeyPress={this.enter}>
					<p>
						The Username {this.props.getUsername} already Exist!!!!
					</p>
					<input type='button' value='Add anyways!' onClick={this.props.addFunction} className='buttons' />&ensp;
					<input type='reset' value="Don't Add!" onClick={this.cancel} className='buttons' />
				</div>
			</div>
			);
	}
}
