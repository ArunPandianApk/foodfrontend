import React from 'react';
import './App.css';

export default class AdminHeader extends React.Component
{
	logout = () =>
	{
		fetch("http://ajira-foodbackend.herokuapp.com/superuser/logout");
		window.location='/';
	}
	changePassword = () =>
	{
		window.location = '/changepassword/1';
	}

	render()
	{
		return(
			<div id='header'>
				<strong>Ajira</strong>
				<div id='adminhead'>
					<input type='button' value='Logout' id='logoutbutton' className='headerbutton' onClick={this.logout} />
					<input type='button' id='changepasswordbutton' className='headerbutton' value='Change Password' onClick={this.changePassword} />
				</div>
			</div>
			)
	}
}
