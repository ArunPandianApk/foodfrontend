import React from 'react';
import AdminHeader from './AdminHeader.js';
import Util from './Util.js';
import './App.css';

export default class EditUsers extends React.Component
{
  state = {
    isAuthenticated: false
  }

  componentWillMount()
	{
		var that = this;
		fetch('http://ajira-foodbackend.herokuapp.com/superuser/check/1', { credentials: "include" })
	    .then(response => response.text())
	    .then(function(response)
	    {
	    	if(response === "true")
          that.getUsersList();
  			else
  				window.location = '/login';
	    });
	}

  getUsersList = async () =>
  {
    var that = this;
    await fetch('http://ajira-foodbackend.herokuapp.com/users/all')
    .then(usersList => usersList.json())
    .then(function(usersList){
      that.setState({
        usersList,
        isAuthenticated: true
      });
    });
  }

  checkPassword = async () =>
  {
    var that = this;
    await fetch('http://ajira-foodbackend.herokuapp.com/superuser/1')
          .then(admin => admin.json())
          .then(function(admin)
          {
            if(admin.password === Util.getValue('edituserpassword'))
              that.changeUser();
            else
            {
              fetch('http://ajira-foodbackend.herokuapp.com/superuser/logout')
              .then(function(){
                window.location = '/login';
                alert('Wrong Admin Password!!! You are being redirected to Login Page!!!');
              });
            }
          });
  }

  getObject = () =>
  {
    return {
      id: Util.getValue('edituserid'),
      name: Util.getValue('neweditname'),
			isActive: true
    }
  }

  changeUser = () =>
  {
    fetch("http://ajira-foodbackend.herokuapp.com/users/add",
		{
	      	method: 'POST',
	      	headers:
	      	{
	        	'Content-Type' : 'application/json'
	      	},
	      	body: JSON.stringify(this.getObject())
	    })
      .then(function()
      {
        alert('Username changed to '+Util.getValue('neweditname')+'!!!!');
        window.location = '/edituser';
      });
  }

  onEnter = (event) =>
  {
    if(event.key === 'Enter')
			this.checkPassword();
  }

  back = () =>
  {
    window.location = '/admin';
  }

  render()
  {
    if(this.state.isAuthenticated)
    {
      return (
        <div id='edituserpage'>
          <AdminHeader />
          <div id='edituser' onKeyPress={this.onEnter}>
            <h3>Edit Users</h3>
            Select User:&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;
            <select id='edituserid'>
            <option />
            {
                this.state.usersList.map((user,i) => <option key={i} value={user.id}>{user.name}</option>)
            }
            </select>
            <br /><br />
            Enter new name:&ensp;&ensp;<input type='text' id='neweditname' />
            <br /><br />
            Admin Password:&ensp;<input type='password' className='textbox' id='edituserpassword' />
            <br /><br />
            <input type='button' className='buttons' value='Confirm' onClick={this.checkPassword} />&ensp;
            <input type='button' className='buttons' value='Back' onClick={this.back} />
          </div>
        </div>
        );
      }
      return <h3>Loading.....</h3>;
  }
}
