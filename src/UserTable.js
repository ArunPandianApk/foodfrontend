import React from 'react';
import UserTableContent from './UserTableContent.js';
import './App.css';

export default class UserTable extends React.Component
{
	displayData = () =>
	{
		if(this.props.list !== null)
			return this.props.list.map((user,i) => <UserTableContent key={i} data={user} manipulate={this.props.manipulate} />);
		return (<tr><td colSpan={3}>No data to Display</td></tr>);
	}

	render()
	{
		return(
			<table>
				<thead>
					<tr>
						<th>User Name</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				{ this.displayData() }
				</tbody>
			</table>
			);
	}
}
