import React from 'react';
import Form from './Form.js';
import Header from './Header.js';
import './App.css';

class Home extends React.Component
{
	componentWillMount()
	{
		fetch('http://ajira-foodbackend.herokuapp.com/orders/session')
		.then(function(session) {return session.json()})
		.then(session => this.setState({session}));
		fetch('http://ajira-foodbackend.herokuapp.com/users/name')
				.then(response => response.json())
				.then(nameList => this.setState({nameList}));
	}

	getCurrentDate = () =>
	{
		var date=new Date();
		date.setUTCHours(0,0,0,0);
		return date.toISOString().substring(0,10);
	}

	navigate = () =>
	{
			if(!this.state.session.isEnded)
			{
				if(this.state.session.date === this.getCurrentDate())
					return <Form nameList={this.state.nameList}/>;
				else
					window.location = '/vendorlogin';
			}
			else
				window.location = "/endedmessage";
	}

	render()
	{
		if(this.state !== null && this.state.session !== undefined && this.state.nameList !== undefined)
		{
			return(
				<div id='homepage'>
				<Header />
				{
					this.navigate()
				}
				</div>
			);
		}
		return(
			<div>
				<h1>Loading.......</h1>
			</div>
			);
	}
}

export default Home;
